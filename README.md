# PWr Eduroam WiFi Setup 

A brief guide on how to connect to the publicly available *eduroam* network on Linux systems using *NetworkManager*

## Getting started

In one of the newer versions of *OpenSSL*, support for TLS 1.0 protcol has been disabled. 
Since then it is not possible to connect to the *eduroam* network within Politechnika Wrocławska grounds. 
Authorization servers only support outdated protocol and authorization using the TLS 1.1 or TLS 1.2

## Wifi Setup 

### Update your login and password

Replace active directory login and password in `eduroam.nmconnection` with your credentials.
Active Directory login isn't your index number. It consists of the first three characters of your name and four random numbers (ex. jankow1234). 
The Active Directory login can be checked at [AD Account Details](https://login.pwr.edu.pl/auth/realms/pwr.edu.pl/account/)

### Copy connection configuration 
After completing the login and password, copy the file to the configuration directory of NetworkManager
Depending on your distribution, configured files may be kept in other places.

#### ArchLinux

```bash
cp eduroam.nmconnection /etc/NetworkManager/system-connections/
```

### Restart NetworkManager

After copying the file, you may need to restart the *NetworkManager* service

```bash 
sudo systemctl restart NetworkManager
```

From this point it should be possible to connect to the *eduroam* network.
